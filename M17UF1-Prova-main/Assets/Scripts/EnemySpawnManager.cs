
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class EnemySpawnManager : MonoBehaviour
{
    [SerializeField] GameObject _missile;
    private float _whenItSpawns;
    private float _howMuchTimeToSpawn;

    // Start is called before the first frame update
    void Start()
    {
        _whenItSpawns = 1;
        _howMuchTimeToSpawn = _whenItSpawns;
    }

    // Update is called once per frame
    void Update()
    {
        _howMuchTimeToSpawn -= Time.deltaTime;
        if (_howMuchTimeToSpawn < 0)
        {
            Instantiate(_missile, new Vector3(transform.position.x, transform.position.y, transform.position.z), new Quaternion(0, 0, 0, 0));
            _howMuchTimeToSpawn = _whenItSpawns;
        }
    }
}

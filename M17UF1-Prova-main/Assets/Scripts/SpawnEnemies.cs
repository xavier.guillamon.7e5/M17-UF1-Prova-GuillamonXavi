using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemies : MonoBehaviour
{
    [SerializeField]
    private GameObject[] enemies;
    private float _whenItSpawns;
    private float _howMuchTimeToSpawn;

    // Start is called before the first frame update
    void Start()
    {
        _whenItSpawns = 3;
        _howMuchTimeToSpawn = _whenItSpawns;
    }

    // Update is called once per frame
    void Update()
    {
        _howMuchTimeToSpawn -= Time.deltaTime;
        if (_howMuchTimeToSpawn < 0)
        {
            Instantiate(enemies[Random.Range(0, 1)], new Vector3(Random.Range(-9f, 9f), transform.position.y, transform.position.z), new Quaternion(0, 0, 0, 0));
            _howMuchTimeToSpawn = _whenItSpawns;
        }
    }
}

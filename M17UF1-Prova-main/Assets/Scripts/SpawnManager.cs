using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SpawnManager : MonoBehaviour
{
    [SerializeField]
    private GameObject missile;

    // Update is called once per frame
    void Update()
    {        
        if (Input.GetKeyUp(KeyCode.Space))
        {
            Instantiate(missile, new Vector3(transform.position.x, transform.position.y, transform.position.z), new Quaternion(0, 0, 0, 0));
        }
    }
}

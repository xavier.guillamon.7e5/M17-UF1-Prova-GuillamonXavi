using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    [SerializeField]
    private int enemiesDownCounter = 0;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("GameManager is NULL");
            }
            return _instance;
        }
    }
    private void Awake()
    {
        if (_instance != null && _instance != this) Destroy(this.gameObject);
        _instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void IncreaseEnemiesDown()
    {
        enemiesDownCounter += 5;
    }
    public void IncreaseEvenMoreEnemiesDown()
    {
        enemiesDownCounter += 20;
    }
    public int GetEnemiesDown()
    {
        return enemiesDownCounter;
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienFragate : MonoBehaviour
{
    private int lives = 5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("PlayerMissile"))
        {
            lives--;
            if (lives == 0)
            {
                GameManager.Instance.IncreaseEvenMoreEnemiesDown();
                Debug.Log(GameManager.Instance.GetEnemiesDown());
                Destroy(this.gameObject);
            }
        }
        if (collider.gameObject.layer == LayerMask.NameToLayer("Walls"))
        {
            var rb = GetComponent<Rigidbody2D>();
            rb.gravityScale = 0f;
            rb.velocity = new Vector2(0, 0);
            Destroy(this.gameObject);
        }
    }
}

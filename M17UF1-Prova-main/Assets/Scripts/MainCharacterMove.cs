using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainCharacterMove : MonoBehaviour
{
    [SerializeField] private float _speed;
    private float horizontal;
    private float vertical;
    private float diagnolSpeedLimit = 0.7f;

    private Transform body;
    void Start()
    {
        body = transform;
    }

    void Update()
    {
        InputCollection();
        Movement();
    }

    private void InputCollection()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");
    }

    private void Movement()
    {
        float horizontalSpeed = horizontal * _speed;
        float verticalSpeed = vertical * _speed;

        if(horizontal != 0 && vertical != 0)
        {
            body.position += new Vector3(horizontalSpeed * diagnolSpeedLimit * Time.deltaTime, verticalSpeed * diagnolSpeedLimit * Time.deltaTime, 0f);
        } else
        {
            body.position += new Vector3(horizontalSpeed * Time.deltaTime, verticalSpeed * Time.deltaTime, 0f);
        }
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Enemy"))
        {
            SceneManager.LoadScene("MenuFinal");
        }
    }

}
